package com.packt.example.authserverjwt.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.packt.example.authserverjwt.entity.Post;
import com.packt.example.authserverjwt.entity.User;
import com.packt.example.authserverjwt.repo.UserRepository;
import com.packt.example.authserverjwt.repo.UserResourceDetails;

@Service
//@Transactional(readOnly = true)
public class UserService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	
	@Override
	public UserDetails loadUserByUsername(String uname) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(uname).orElseThrow(() -> new RuntimeException());
		return new UserResourceDetails(user);
	}
	
	public List<Post> findByUserid(String userid)
	{

		User user= userRepository.findByUserid(userid);
		
		return user.getPosts();

	}
	
	
//	@Transactional(rollbackFor = Exception.class)
	public User create(User user) {
		return userRepository.save(user);
		
	
	}
}
