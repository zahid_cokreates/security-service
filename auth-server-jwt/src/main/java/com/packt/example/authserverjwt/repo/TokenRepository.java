package com.packt.example.authserverjwt.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.packt.example.authserverjwt.entity.Tokens;



public interface TokenRepository extends JpaRepository<Tokens, Long>{

	@Query("select t from Tokens t where t.token =:token")
	Tokens findByToken(@Param(value="token") String token);
}
