package com.packt.example.authserverjwt.entity;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;



@Entity
@Table(name="api")
public class Api implements Serializable{
	
	
	@Id
	@Column(name="api_id")
	private String apiid;
	
	@Column(name="api_name")
	private String apiname;
	
	
//	@ManyToMany(fetch=FetchType.LAZY,
//			cascade= {CascadeType.PERSIST, CascadeType.MERGE,
//			 CascadeType.DETACH, CascadeType.REFRESH})
//	@JoinTable(
//			name="post_api",
//			joinColumns=@JoinColumn(name="api_id"),
//			inverseJoinColumns=@JoinColumn(name="post_id")
//			)
//	private List<Post> posts;
	
	

//	public List<Post> getPosts() {
//		return posts;
//	}
//
//	public void setPosts(List<Post> posts) {
//		
//		
//		this.posts = posts;
//	}

	public String getApiid() {
		return apiid;
	}

	public void setApiid(String apiid) {
		this.apiid = apiid;
	}

	public String getApiname() {
		return apiname;
	}

	public void setApiname(String apiname) {
		this.apiname = apiname;
	}

	public Api(String apiid, String apiname) {
		super();
		this.apiid = apiid;
		this.apiname = apiname;
	}
	
	public Api()
	{
		
	}

}
