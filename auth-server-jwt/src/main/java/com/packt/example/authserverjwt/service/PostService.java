package com.packt.example.authserverjwt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.packt.example.authserverjwt.entity.Api;
import com.packt.example.authserverjwt.repo.PostRepository;



@Service
public class PostService{
	
	@Autowired
	private PostRepository postRepository;

	public PostService(PostRepository postRepository) {
		this.postRepository = postRepository;
	}
	
	public List<Api> findByPostId(String postid)
	{
		return postRepository.findByPostid(postid).getApis();
	}

	
}