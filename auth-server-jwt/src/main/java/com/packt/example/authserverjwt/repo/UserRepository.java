package com.packt.example.authserverjwt.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.packt.example.authserverjwt.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	Optional<User> findByUsername(String uname);
	
	@Query("select u from User u where u.username =:username")
	User findByUserid(@Param(value="username") String userid);
	
}

