package com.packt.example.authserverjwt.repo;


import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.packt.example.authserverjwt.entity.User;


public class UserResourceDetails implements UserDetails{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private User wrapped;

	public UserResourceDetails(User wrapped) {
		this.wrapped = wrapped;
	}

	public String getPassword() {
		return wrapped.getPassword();
	}

	public String getUsername() {
		return wrapped.getUsername();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	// other methods hidden for clarity
}
