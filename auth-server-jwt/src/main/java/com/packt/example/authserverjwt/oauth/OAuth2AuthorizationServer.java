package com.packt.example.authserverjwt.oauth;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.packt.example.authserverjwt.service.AdditionalClaimsTokenEnhancer;

@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServer extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private DataSource dataSource;
	
	 @Autowired
	 private JdbcTemplate jdbcTemplate;
	 
	 @Autowired
     private AdditionalClaimsTokenEnhancer enhancer;
	 
	 @Autowired
	 private BCryptPasswordEncoder encoder;

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey("non-prod-signature"); 
        return converter;
    }

    
//	@Bean
//	public JwtAccessTokenConverter accessTokenConverter() {
//		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//		try {
//			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
//
//			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
//
//			keyGen.initialize(1024, random);
//			KeyPair keyPair = keyGen.generateKeyPair();
//
//			converter.setKeyPair(keyPair);
//
//		} catch (Exception e) {
//			throw new RuntimeException(e);
//		}
//		return converter;
//	}

	@Bean
	public JwtTokenStore jwtTokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
	
	@Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

//	@Bean
//	public TokenStore tokenStore() {
//		
//		return new JdbcTokenStore(jdbcTemplate.getDataSource());
//	}
	
//	  	@Bean
//	    public TokenStore tokenStore() {
//	        String insertAccessTokenSql = "insert into oauth_access_token (token_id, token, authentication_id, user_name, client_id, authentication, refresh_token) values (?, ?, ?, ?, ?, ?, ?)";
////	        String selectAccessTokensFromUserNameAndClientIdSql = "select token_id, token from oauth_access_token where email = ? and client_id = ?";
////	        String selectAccessTokensFromUserNameSql = "select token_id, token from oauth_access_token where email = ?";
////	        String selectAccessTokensFromClientIdSql = "select token_id, token from oauth_access_token where client_id = ?";
//	        String insertRefreshTokenSql = "insert into oauth_refresh_token (token_id, token, authentication) values (?, ?, ?)";
//
//	        JdbcTokenStore jdbcTokenStore = new JdbcTokenStore(dataSource);
//	        jdbcTokenStore.setInsertAccessTokenSql(insertAccessTokenSql);
////	        jdbcTokenStore.setSelectAccessTokensFromUserNameAndClientIdSql(selectAccessTokensFromUserNameAndClientIdSql);
////	        jdbcTokenStore.setSelectAccessTokensFromUserNameSql(selectAccessTokensFromUserNameSql);
////	        jdbcTokenStore.setSelectAccessTokensFromClientIdSql(selectAccessTokensFromClientIdSql);
//	        jdbcTokenStore.setInsertRefreshTokenSql(insertRefreshTokenSql);
//
//
//	        return jdbcTokenStore;
//	    }

//	@Bean
//	public ApprovalStore approvalStore() {
//		return new JdbcApprovalStore(jdbcTemplate.getDataSource());
//	}

//    @Override
//    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
//    	
////    	TokenEnhancerChain chain = new TokenEnhancerChain();
////        chain.setTokenEnhancers(
////           Arrays.asList(enhancer, accessTokenConverter()));
//        
//        endpoints
//           .authenticationManager(authenticationManager)
//           .tokenStore(tokenStore())
//           .approvalStore(approvalStore())
//           .accessTokenConverter(accessTokenConverter());
//        
//
//
//    }
    
   

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		
		
//		 TokenEnhancerChain chain = new TokenEnhancerChain();
//         chain.setTokenEnhancers(
//         Arrays.asList(enhancer, accessTokenConverter()));
		
//      System.out.println("-------------chain--------"+chain);
		
		endpoints.authenticationManager(authenticationManager)
				.tokenStore(enhancer)
				.accessTokenConverter(accessTokenConverter());
		
		
		
		
	}

//	@Override
//	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
//		endpoints.approvalStore(approvalStore()).tokenStore(tokenStore());
//	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()");
		security.checkTokenAccess("permitAll()");
		
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		
		clients.inMemory().withClient("clientapp").secret(encoder.encode("123456")).scopes("read_profile")
				.authorizedGrantTypes("password", "authorization_code", "refresh_token");
				
				
		
		
		
//		clients
//        .jdbc(jdbcTemplate.getDataSource());
	}

}
