package com.packt.example.authserverjwt.oauth;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;


import org.springframework.core.annotation.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Order(1)
public class TransactionFilter implements Filter {

	@Override
    public void doFilter
	(ServletRequest request, ServletResponse response,
	FilterChain chain)throws IOException,ServletException
	{

		HttpServletRequest req = (HttpServletRequest) request;
		System.out.println("filter");
		System.out.println(req.getRequestURI());
//		String token=req.getHeader("Authorization").replace("Bearer ","");
//		System.out.println("---Token-------"+token);
//		String apiName=req.getRequestURI().replace("/api/","");
//		System.out.println("URL"+apiName);
		
		
//		RestTemplate restTemplate = new RestTemplate();
//		  
//		  String url
//		    = "http://localhost:8085/authorize/{api}";
//		  
//		  HttpHeaders headers = new HttpHeaders();
//		 // headers.set("Authorization", "Bearer "+token);
//		  headers.setContentType(MediaType.TEXT_PLAIN);
//		  
//		   HttpEntity<String> entity = new HttpEntity<String>("Authorization: Bearer "+token, headers);
//		  
////		  Map<String, String> params = new HashMap<String, String>();
////		    params.put("api", apiName);
//		    
//		    System.out.println(entity.toString());
//		  
//		  ResponseEntity<String> response1
//		    = restTemplate.exchange(url, HttpMethod.GET, entity, String.class,apiName);
//		  System.out.println(response1);

		chain.doFilter(request, response);
		
	}

	

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}


	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	

	// other methods
}
