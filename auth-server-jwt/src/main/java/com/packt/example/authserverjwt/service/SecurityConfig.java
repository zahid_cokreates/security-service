package com.packt.example.authserverjwt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserService users;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

   
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(users).passwordEncoder(bCryptPasswordEncoder);
        
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        
        http.
        authorizeRequests()
        .antMatchers("/authorize/**").permitAll();
       
        
//    	http.authorizeRequests()
//        .anyRequest().fullyAuthenticated()
//        .antMatchers(HttpMethod.GET, "/**").permitAll()
//     .and()
//        .httpBasic()
//     .and()
//        .csrf().disable();
//        http
//        .authorizeRequests()
//        .antMatchers("/authorize/**").permitAll()
//        .anyRequest().authenticated();
        
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/authorize/**");
    }
    
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
    	return super.authenticationManagerBean();
    }

}
