package com.packt.example.authserverjwt.service;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import com.packt.example.authserverjwt.entity.Tokens;
import com.packt.example.authserverjwt.repo.UserResourceDetails;

@Component
public class AdditionalClaimsTokenEnhancer implements TokenStore {
	
	
	
	
	@Autowired
	private TokenService tokenDetailService;
	
//	@Override
//	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
//		Map<String, Object> additional = new HashMap<>();
//		UserResourceDetails user = (UserResourceDetails) authentication.getPrincipal();
//		System.out.println("access_token"+accessToken);
//		DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) accessToken;
//		token.setExpiration(new Date(System.currentTimeMillis()+500000));
//		token.setAdditionalInformation(additional);
//		System.out.println("---------------Token--------------------------");
//		System.out.println(token);
//		
//		
//		System.out.println("---------------Date--------------------------");
//		System.out.println(token.getExpiration());
//		
//		acc_token=(DefaultOAuth2AccessToken) accessToken;
//		System.out.println("---------------ID--------------------------");
//		System.out.println(authentication.getName());
//		id=authentication.getName();
//		
//		java.sql.Date expire =new java.sql.Date(token.getExpiration().getTime());
//		
//		
//		tokenDetailService.create(new Tokens(id,acc_token.toString(),expire));
//		
//		return accessToken;
//	}

	@Override
	public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OAuth2Authentication readAuthentication(String token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		
		java.sql.Date expire =new java.sql.Date(token.getExpiration().getTime());
		Jwt jwtToken = JwtHelper.decode(token.toString());
	    String claims = jwtToken.getClaims();
	   // System.out.println("-----claims---------"+claims+"cl"+ token.toString().substring(token.toString().indexOf("client_id")+13,token.toString().indexOf("client_id")+22));
		tokenDetailService.create(new Tokens(authentication.getName(),token.toString(),claims.substring(claims.indexOf("client_id")+12,claims.indexOf("client_id")+21),expire));

		System.out.println("access_token"+token);
	}

	@Override
	public OAuth2AccessToken readAccessToken(String tokenValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeAccessToken(OAuth2AccessToken token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public OAuth2RefreshToken readRefreshToken(String tokenValue) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeRefreshToken(OAuth2RefreshToken token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
		// TODO Auto-generated method stub
		return null;
	}
	

	

}
