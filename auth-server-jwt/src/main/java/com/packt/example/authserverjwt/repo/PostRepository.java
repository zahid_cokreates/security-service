package com.packt.example.authserverjwt.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.packt.example.authserverjwt.entity.Post;


public interface PostRepository extends JpaRepository<Post, Long>{
	
	@Query("select p from Post p where p.postid =:postid")
	Post findByPostid(@Param(value="postid") String postid);

}
