package com.packt.example.authserverjwt.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.packt.example.authserverjwt.entity.Tokens;
import com.packt.example.authserverjwt.repo.TokenRepository;


@Service
@Transactional(readOnly = true)
public class TokenService {
	
	@Autowired
	private final TokenRepository tokenRepository;

	public TokenService(TokenRepository tokenRepository) {
		this.tokenRepository = tokenRepository;
	}

	@Transactional(rollbackFor = Exception.class)
	public Tokens create(Tokens token) {
		return tokenRepository.save(token);
	}
	
	public Tokens findByToken(String token)
	{
		return tokenRepository.findByToken(token);
	}

}
