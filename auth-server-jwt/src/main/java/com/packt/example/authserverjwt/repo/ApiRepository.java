package com.packt.example.authserverjwt.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.packt.example.authserverjwt.entity.Api;


public interface ApiRepository extends JpaRepository<Api, Long>{
	

}
