package com.packt.example.authserverjwt.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.packt.example.authserverjwt.entity.Api;
import com.packt.example.authserverjwt.entity.Post;
import com.packt.example.authserverjwt.entity.Tokens;
import com.packt.example.authserverjwt.service.PostService;
import com.packt.example.authserverjwt.service.TokenService;
import com.packt.example.authserverjwt.service.UserService;

@Controller
public class AuthController {

	@Autowired
	private UserService userService;

	@Autowired
	private PostService postService;

	@Autowired
	private TokenService tokenService;
	
	@RequestMapping(value = "/authorize/hello", method = RequestMethod.GET)
	public ResponseEntity<String> getHello() {
		
		return ResponseEntity.ok("say hello");
	}
	
	

	@RequestMapping(value = "/authorize/{api}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getAuth(@RequestHeader(value = "Authorization") String header,
			@PathVariable("api") String api) {

		System.out.println("passed");
		System.out.println("header" + header + "api" + api);

//		Jwt jwtToken = JwtHelper.decode(header.replace("Bearer ", ""));
//	    String claims = jwtToken.getClaims();

		// System.out.println("jti "+claims);
		System.out.println(header.replace("Bearer ", ""));
		Tokens token;
		token = tokenService.findByToken(header.replace("Bearer ", ""));
		if (token == null) {
			return new ResponseEntity<>("Token is invalid", HttpStatus.BAD_REQUEST);
		}
		
		String id = token.getUser_id();

//	    System.out.println(claims.substring(claims.indexOf("jti")+6, claims.indexOf("jti")+42));

//		String url = "http://localhost:8085/oauth/check_token";
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//
//		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
//		map.add("token", header.replace("Bearer ", ""));
//
//		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//
//		RestTemplate restTemplate = new RestTemplate();
//		ResponseEntity<String> response = restTemplate.postForEntity( url, request , String.class );
//		System.out.println("response"+response.getBody());
//		
//		System.out.println(response.getStatusCode());
//		
//		System.out.println("response "+response.getBody().charAt(response.getBody().indexOf("user_name")+12));
//		
//		String id= Character.toString(response.getBody().charAt(response.getBody().indexOf("user_name")+12));
//		System.out.println("id "+ id);

		List<Post> posts = userService.findByUserid(id);

		System.out.println("passed");
		int matched = 0;
		for (Post post : posts) {
			System.out.println(post.getPostid());

			List<Api> apis = postService.findByPostId(post.getPostid());
			System.out.println("passed");
			for (Api apiName : apis) {
				System.out.println(apiName.getApiname());
				if (apiName.getApiname().matches(api)) {
					System.out.println("found");
					matched = 1;
					break;

				}

			}

		}

//		String url = "http://localhost:8085/oauth/check_token";
//
//		
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("token",header.replace("Bearer ", "") );
//
//		RestTemplate restTemplate = new RestTemplate();
//		ResponseEntity<String> response = restTemplate.postForEntity( url, params, String.class );

		if (matched == 1) {
			return new ResponseEntity<>("Found", HttpStatus.OK);
		} else {
			System.out.println("not found");
			return new ResponseEntity<>("Invalid Api", HttpStatus.BAD_REQUEST);
			//return new ResponseEntity<>("Token is invalid", HttpStatus.BAD_REQUEST);
		}

	}

}
