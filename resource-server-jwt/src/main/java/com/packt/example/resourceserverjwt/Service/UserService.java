package com.packt.example.resourceserverjwt.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.packt.example.resourceserverjwt.Repository.UserRepository;
import com.packt.example.resourceserverjwt.entity.Post;
import com.packt.example.resourceserverjwt.entity.User;


@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	public List<Post> findByUserid(String userid)
	{

		User user= userRepository.findByUserid(userid);
		
		return user.getPosts();

	}

}
