package com.packt.example.resourceserverjwt.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.packt.example.resourceserverjwt.Repository.PostRepository;
import com.packt.example.resourceserverjwt.entity.Api;

@Service
public class PostService{
	
	@Autowired
	private PostRepository postRepository;

	public PostService(PostRepository postRepository) {
		this.postRepository = postRepository;
	}
	
	public List<Api> findByPostId(String postid)
	{
		return postRepository.findByPostid(postid).getApis();
	}
}
