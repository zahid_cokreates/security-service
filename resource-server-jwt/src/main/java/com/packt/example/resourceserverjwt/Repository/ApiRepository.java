package com.packt.example.resourceserverjwt.Repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.packt.example.resourceserverjwt.entity.Api;


public interface ApiRepository extends JpaRepository<Api, Long>{
	
	
	

}
