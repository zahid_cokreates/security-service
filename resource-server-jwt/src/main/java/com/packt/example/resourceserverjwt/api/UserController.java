package com.packt.example.resourceserverjwt.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.packt.example.resourceserverjwt.Service.UserService;
import com.packt.example.resourceserverjwt.entity.Post;
import com.packt.example.resourceserverjwt.entity.TransactionFilter;


@Controller
public class UserController {
	
//	@Autowired
//	private UserService userService;
	
	
	
	@Bean
	public FilterRegistrationBean<TransactionFilter> loggingFilter(){
	    FilterRegistrationBean<TransactionFilter> registrationBean 
	      = new FilterRegistrationBean<>();
	         
	    registrationBean.setFilter(new TransactionFilter());
	    registrationBean.addUrlPatterns("/api/*");
	         
	    return registrationBean;    
	}

	//@PreAuthorize("isMember('add_employee')")
    @RequestMapping(value="/api/add_employee")
    public ResponseEntity<UserProfile> myProfile() {
        String username = (String) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        
        String email = username + "added employee";

        UserProfile profile = new UserProfile(username, email);

        return ResponseEntity.ok(profile);
    }
	
	//@PreAuthorize("isMember('add_lead')")
    @RequestMapping("/api/add_lead")
    public ResponseEntity<UserProfile> lead() {
        String username = (String) SecurityContextHolder.getContext()
                .getAuthentication().getName();
        String email = username + " added lead";

        UserProfile profile = new UserProfile(username, email);

        return ResponseEntity.ok(profile);
    }
	
//	@RequestMapping("/api")
//	public ResponseEntity<List<Post>> api()
//	{
//		String id = (String) SecurityContextHolder.getContext()
//                .getAuthentication().getPrincipal();
//
//		return new ResponseEntity<List<Post>>(userService.findByUserid(id), HttpStatus.OK);
//		
//
//	}
	

}
