package com.packt.example.resourceserverjwt.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {

	@Id
	@Column(name="user_id")
	private String userid;

	@Column(name="user_name")
	private String publicname;

	@Column(name="user_pass")
	private String password;

	@Column(name="user_email")
	private String email;

	
	@ManyToMany(fetch=FetchType.LAZY,
			cascade= {CascadeType.PERSIST, CascadeType.MERGE,
			 CascadeType.DETACH, CascadeType.REFRESH})
	@JoinTable(
			name="user_post",
			joinColumns=@JoinColumn(name="user_id"),
			inverseJoinColumns=@JoinColumn(name="post_id")
			)
	private List<Post> posts;
	

	public String getUserid() {
		return userid;
	}






	public void setUserid(String userid) {
		this.userid = userid;
	}






	public List<Post> getPosts() {
		return posts;
	}






	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}






	public User(String username, String publicname, String password, String email) {
		super();
		this.userid= username;
		this.publicname = publicname;
		this.password = password;
		this.email = email;
	}



	


	public String getUsername() {
		return userid;
	}



	public void setUsername(String username) {
		this.userid = username;
	}



	public String getPublicname() {
		return publicname;
	}



	public void setPublicname(String publicname) {
		this.publicname = publicname;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}



	public User() {

	}

	

}

