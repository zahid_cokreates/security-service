package com.packt.example.resourceserverjwt.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.packt.example.resourceserverjwt.entity.Post;
import com.packt.example.resourceserverjwt.entity.User;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>{
	
	@Query("select p from Post p where p.postid =:postid")
	Post findByPostid(@Param(value="postid") String postid);

}
